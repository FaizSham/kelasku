<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar Bootstrap</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
        crossorigin="anonymous"></script>

    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="/img/gambar1.jpg" alt="" width="30" height="30" class="d-inline-block align-text-top">
                KelasProgramming.com
            </a>
        </div>
    </nav>

    <!--hero area -->
    <div class="hero-area">

        <div class="container">
            <div class="row">
                <div class="col">
                    <h1>KelasProgramming.com</h1>
                    <p>Program pembelajaran PHP</p>
                </div>
            </div>
        </div>
    </div>


    <div class="container mt-5">
        <!--subtitle-->
        <div class="row">
            <div class="col text-center">
                <h3>Gambar-gambar Pekan</h3>
            </div>

        </div>

        <!-- gallery -->
        <div class="row">

        <?php foreach($all_pekan as $pekan ) { ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-3">
                <div class="card">
                    <img src="/img/<?php echo $pekan->nama_fail?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5><?php echo $pekan->nama;?></h5>
                        <p class="card-text"><?php echo $pekan->keterangan; ?></p>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div><!-- /row -->

        <!--pagination-->

        <div class="row p-5">
            <div class="col-12">

                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div><!-- /pagination -->

    </div><!-- /container -->

<footer class="text-center p-5">
    <p>Hakcipta Terpelihara &copy; 2021</p>
</footer>


</body>

</html>