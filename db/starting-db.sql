-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table kelasku.auth_logins
CREATE TABLE IF NOT EXISTS `auth_logins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `successfull` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table kelasku.auth_logins: 4 rows
/*!40000 ALTER TABLE `auth_logins` DISABLE KEYS */;
INSERT INTO `auth_logins` (`id`, `user_id`, `firstname`, `lastname`, `role`, `ip_address`, `date`, `successfull`) VALUES
	(1, 1, 'John', 'Doe', '1', '127.0.0.1', '2023-08-29 03:43:31', 1),
	(2, 1, 'John', 'Doe', '1', '127.0.0.1', '2023-08-29 03:46:09', 1),
	(3, 1, 'John', 'Doe', '1', '127.0.0.1', '2023-08-29 03:53:52', 1),
	(4, 1, 'John', 'Doe', '1', '127.0.0.1', '2023-08-29 08:39:43', 1);
/*!40000 ALTER TABLE `auth_logins` ENABLE KEYS */;

-- Dumping structure for table kelasku.auth_tokens
CREATE TABLE IF NOT EXISTS `auth_tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `selector` varchar(255) NOT NULL,
  `hashedvalidator` varchar(255) NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table kelasku.auth_tokens: 0 rows
/*!40000 ALTER TABLE `auth_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_tokens` ENABLE KEYS */;

-- Dumping structure for table kelasku.gambar
CREATE TABLE IF NOT EXISTS `gambar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `nama_fail` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table kelasku.gambar: ~13 rows (approximately)
INSERT INTO `gambar` (`id`, `nama`, `nama_fail`, `keterangan`) VALUES
	(1, 'Rembau', 'rembau.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque cupiditate corrupti temporibus consectetur deserunt porro eligendi pariatur facere reiciendis! Modi illo earum quo a? Repellendus accusantium exercitationem amet aliquid culpa.'),
	(2, 'Gemas', 'gemas.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque cupiditate corrupti temporibus consectetur deserunt porro eligendi pariatur facere reiciendis! Modi illo earum quo a? Repellendus accusantium exercitationem amet aliquid culpa.'),
	(3, 'Seremban', 'seremban.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque cupiditate corrupti temporibus consectetur deserunt porro eligendi pariatur facere reiciendis! Modi illo earum quo a? Repellendus accusantium exercitationem amet aliquid culpa.'),
	(4, 'Juaseh', 'juaseh.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque cupiditate corrupti temporibus consectetur deserunt porro eligendi pariatur facere reiciendis! Modi illo earum quo a? Repellendus accusantium exercitationem amet aliquid culpa.'),
	(5, 'Kuala Pilah', 'kuala-pilah.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque cupiditate corrupti temporibus consectetur deserunt porro eligendi pariatur facere reiciendis! Modi illo earum quo a? Repellendus accusantium exercitationem amet aliquid culpa.'),
	(6, 'Port Dickson', 'port-dickson.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque cupiditate corrupti temporibus consectetur deserunt porro eligendi pariatur facere reiciendis! Modi illo earum quo a? Repellendus accusantium exercitationem amet aliquid culpa.'),
	(7, 'Jelebu', '1692544381_94c6e960bc867a260ea0.jpg', 'Tempat di Negeri Sembilan'),
	(8, 'Jempol', '1692328625_846a7f173369ed52e94f.jpg', 'Daerah dalam Negeri Sembilan'),
	(9, 'Ayer Keroh', '1692329204_f43009a6697334b51016.jpg', 'Ini di Melaka'),
	(10, 'Alor Gajah', '1692621100_9afa250662a85b52ed8b.jpg', 'Berdekatan daerah Naning'),
	(11, 'Tampin', '1692621608_4a80d3d9269d5957da8e.jpg', 'Dalam Negeri Sembilan'),
	(12, 'Bahau', '1692621689_84a384160fdf1622471d.jpg', 'Di Negeri Sembilan'),
	(14, 'Setia Alam', '1692622997_bc60f40cf6a68f390935.jpg', 'Di Alam Budiman');

-- Dumping structure for table kelasku.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reset_token` varchar(250) NOT NULL,
  `reset_expire` datetime DEFAULT NULL,
  `activated` tinyint(1) NOT NULL,
  `activate_token` varchar(250) DEFAULT NULL,
  `activate_expire` varchar(250) DEFAULT NULL,
  `role` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table kelasku.users: 1 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `reset_token`, `reset_expire`, `activated`, `activate_token`, `activate_expire`, `role`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'John', 'Doe', 'johndoe@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$TjlGb1lGNk5BREp6bWpUeA$Q9UFd/KvaqLMqtQjKuQ6jbM0J3+BrD2REYCUceDyxeE', '', NULL, 1, NULL, NULL, 1, '2023-08-22 23:12:57', '2023-08-22 23:12:57', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table kelasku.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table kelasku.user_roles: 0 rows
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
